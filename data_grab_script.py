import simple_functions as sf
import time


# @sf.check_runtime
def get_land_register_information(driver, division_code="GD1G", land_register_number="00000000", control_number="3"):
    """
    :param division_code:
    :param land_register_number:
    :param control_number:
    :return:

    >>> print(get_land_register_information(land_register_number='00000000', control_number='3'))
    ['Wynik wyszukiwania księgi wieczystej', 'Księga o numerze: GD1G / 00000000 / 3 nie została odnaleziona.']
    """
    assert "EUKW" in driver.title

    # <input id="kodWydzialuInput" class="required error-field error-required" type="text" maxlength="4">
    insert_code = driver.find_element_by_id("kodWydzialuInput")
    insert_code.send_keys(division_code)
    time.sleep(2)
    # <input id="numerKsiegiWieczystej" name="numerKw" class="required only-digits error-field error-required"
    # type="text" value="" maxlength="8">
    insert_code_2 = driver.find_element_by_id("numerKsiegiWieczystej")
    insert_code_2.send_keys(land_register_number)
    time.sleep(2)
    # <input id="cyfraKontrolna" name="cyfraKontrolna"
    # class="required only-digits cyfra-kontrolna blink error-field error-required" type="text" value="" maxlength="1">
    insert_code_3 = driver.find_element_by_id("cyfraKontrolna")
    insert_code_3.send_keys(control_number)
    time.sleep(2)
    # <button id="wyszukaj" name="wyszukaj" class="right" type="submit" value="Submit">Wyszukaj Księgę</button>
    button_1 = driver.find_element_by_id("wyszukaj")
    time.sleep(2)
    button_1.click()

    # <p class="h3">Wynik wyszukiwania księgi wieczystej</p>
    heading = driver.find_element_by_class_name('h3')
    if heading.text == "Wynik wyszukiwania księgi wieczystej":

        form_rows = driver.find_elements_by_class_name('form-row')
        text_form_rows = []
        [text_form_rows.append(i.text) for i in form_rows]

        button_1 = driver.find_element_by_id("powrotDoKryterii")
        time.sleep(2)
        button_1.click()

        return text_form_rows
    else:
        pass