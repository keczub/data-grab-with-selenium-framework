import time
import random


def check_control_number(first_seg, second_seg, ctrl_seg):
    """
    :param first_seg: court identifier four characters str e.g. "KR1C"
    :type first_seg: str
    :param second_seg: identifier number
    :type second_seg: str
    :param ctrl_seg: control number to validate two first segments
    :type ctrl_seg: str
    :return: boolean

    >>> check_control_number("KR1C", "00012345", "9")
    True

    >>> check_control_number("KR1C", "00012345", "8")
    False
    """

    # land and mortage register key
    values_dict = {"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "X": 10,
                   "A": 11, "B": 12, "C": 13, "D": 14, "E": 15, "F": 16, "G": 17, "H": 18, "I": 19, "J": 20,
                   "K": 21, "L": 22, "M": 23, "N": 24, "O": 25, "P": 26, "R": 27, "S": 28, "T": 29, "U": 30,
                   "W": 31, "Y": 32, "Z": 33}

    code_dict_first_seg = {"0": 1, "1": 3, "2": 7, "3": 1}
    code_dict_second_seg = {"0": 3, "1": 7, "2": 1, "3": 3, "4": 7, "5": 1, "6": 3, "7": 7}

    first_seg_sum = sum([code_dict_first_seg[str(nr)] * values_dict[i] for nr, i in enumerate(first_seg)])
    second_seg_sum = sum([code_dict_second_seg[str(nr)] * values_dict[i] for nr, i in enumerate(second_seg)])

    if str((first_seg_sum+second_seg_sum) % 10) == ctrl_seg:
        return True
    else:
        return False


def list_all_possible_combinations(division_code="GD1G", control_number=None,
                                   records_number=10_000, max_land_register_number=None):

    """
    :param division_code:   court identifier four characters str e.g. "KR1C"
    :type division_code: str
    :param control_number: validity control number check
    :type control_number: int
    :param records_number:  number of records that function returns
    :type records_number: int
    :param max_land_register_number: upper limit for land register number
    :type records_number: int
    :return: set of strings

     >>> random.seed(2)
     >>> list_all_possible_combinations(division_code="GD1G", records_number=5)
     {'GD1G_48889548_9', 'GD1G_40707799_8', 'GD1G_76196084_8', 'GD1G_49937087_8', 'GD1G_53668769_7'}
    """

    if records_number >= 1_000_000_000:
        return set()
    else:
        correct_combination = set()

        # for land_register_number in range (0, 1_000_000): #COLLECTION SIZE: 116.35MB
        # minimal value "XXXX / 00_000_000 / X"
        # maximal value "XXXX / 99_999_999 / X"
        # minimal value "XXXX / XX_XXXX_XXX / 0"
        # maximal value "XXXX / XX_XXX_XXX / 9"

        # number of possible combinations for one division
        # 10 * 100_000_000 = 1_000_000_000

        while len(correct_combination) < records_number:
            if max_land_register_number is None:
                land_register_number = random.randint(0, 99_999_999)
            else:
                land_register_number = random.randint(0, max_land_register_number)

            if control_number is None:
                control_number = random.randint(0, 9)

            land_register_number_str = str(land_register_number).rjust(8, "0")
            control_number_str = str(control_number)

            if check_control_number(division_code, land_register_number_str, control_number_str):
                correct_combination.add(division_code + "_" + land_register_number_str + "_" + control_number_str)

        return correct_combination


def initial_data_upload(client):

    mongo_insert_data_list = []
    output_list = list_all_possible_combinations()

    for i in output_list:
        i = i.split("_")
        mongo_insert_data_list.append({"time": int(time.time()), "data_grab_flag": "N",
                                       "land_register_number": i[1],  "control_number": i[2],
                                       "data_grab": None})

    new_db = client["land_register_db"]
    new_db.land_register_collection.insert_many(mongo_insert_data_list)


def check_runtime(func):
    def run(*args, **kwargs):
        start_time = time.time()
        func_output = func(*args, **kwargs)
        end_time = time.time()
        print("Czas trwania funkcji '{0}': {1} sec.".format(str(func.__name__), str(round(end_time - start_time, 4))))
        return func_output
    return run
