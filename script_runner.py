import pymongo
import time
import simple_functions as sf
from data_grab_script import get_land_register_information
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class ReadSession:

    def __init__(self):
        self.current_time = int(time.time())
        self.client = pymongo.MongoClient(open("credentials").read().replace('\n', ''))
        self.mydb = self.client["land_register_db"]
        self.mycol = self.mydb["land_register_collection"]
        self.myquery = {"data_grab_flag": "N", "time": {"$lt": self.current_time}}

        self.driver = None
        self.start_chrome_driver()

    def start_chrome_driver(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")

        # Trying to implement changing global ip
        # First option is load a VPN chrome extension
        # chrome_options.add_extension("./touch_vpn.crx")

        # Second option was inspired by post
        # https://stackoverflow.com/questions/59409418/how-to-rotate-selenium-webrowser-ip-address
        # chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
        # chrome_options.add_experimental_option('useAutomationExtension', False)
        # chrome_options.add_argument('--proxy-server=51.75.147.41:3128')

        self.driver = webdriver.Chrome(r"./config/chromedriver.exe", options=chrome_options)
        self.driver.get("https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW")

    def update_collection(self):

        number_of_missing_items = self.mycol.count({"data_grab_flag": "N"})
        print("Number of missing itmes:", number_of_missing_items)
        if number_of_missing_items == 0:
            return "All documents processed"
        failure_number = 0
        for nr, i in enumerate(range(number_of_missing_items)):
            mydoc = self.mycol.find_one(self.myquery)

            time.sleep(2)
            output = get_land_register_information(driver=self.driver,
                                                   land_register_number=mydoc['land_register_number'],
                                                   control_number=mydoc['control_number'])
            if output:
                failure_number = 0
                newvalues = {"$set": {"time": int(time.time()),
                                      "data_grab_flag": "Y",
                                      "data_grab": output}}

                self.mycol.update_one(mydoc, newvalues)
                print(str(nr), " Passed: ", mydoc['land_register_number'], mydoc['control_number'])
            else:
                newvalues = {"$set": {"time": int(time.time())}}
                self.mycol.update_one(mydoc, newvalues)
                print(str(nr), " Failed: ", mydoc['land_register_number'], mydoc['control_number'])
                failure_number += 1
                if failure_number > 10:
                    print("Process finished due to consequent failures")
                    break

        self.client.close()


if __name__ == '__main__':

    #client = pymongo.MongoClient(open("credentials").read().replace('\n', ''))
    #sf.initial_data_upload(client) # used when new random sampled dataset is created

    ReadSession().update_collection()