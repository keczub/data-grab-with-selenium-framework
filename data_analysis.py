import pymongo
import pandas as pd


def clean_data_grab_list(single_record_data_list):

    # Removing repetitive phrase "Wynik wyszukiwania księgi wieczystej" from collected data in list form
    if "Wynik wyszukiwania księgi wieczystej" in single_record_data_list:
        single_record_data_list.remove("Wynik wyszukiwania księgi wieczystej")

    return single_record_data_list

def classify_values(value):
    """
    Classify incoming int values into 100,000 intervals.

    :param value: number from 0 to 100_000_000 range
    :type value: int
    :return: list[str, int]

    >>> classify_values(100_000)
    ('[100000, 200000]', 1)
    >>> classify_values(200_000)
    ('[200000, 300000]', 2)
    """

    # 0 <-> 99_999_999
    # <0; 100_000)
    # <100_000; 200_000)
    # ...
    # <99_900_000; 100_000_000
    group_identifier = int(value / 100_000)
    return [str([group_identifier*100_000, (group_identifier+1)*100_000]), group_identifier]


client = pymongo.MongoClient(open("credentials").read().replace('\n', ''))
mydb = client["land_register_db"]
mycol = mydb["land_register_collection"]
mydoc = mycol.find()

print("Number of documents:", mydoc.count())

df = pd.DataFrame(list(mydoc))
df.land_register_number = pd.to_numeric(df.land_register_number)
df.control_number = pd.to_numeric(df.control_number)

df.sort_values(by=["land_register_number", "control_number"], inplace=True)
df.data_grab.fillna("", inplace=True)
df.data_grab.apply(clean_data_grab_list)

# Analysis assumption that data in list only consists of 1 or 9
assert set(df.data_grab.map(len).value_counts().index.to_list()) == set([0, 1, 9]), "More cases encountered"
df["includes_information"] = df.data_grab.map(len).astype(str).map({"9": "Full information",
                                                                    "1": "Empty information",
                                                                    "0": "Not checked yet"})

df["raw_group"] = df.land_register_number.map(classify_values)
df["group"] = df.raw_group.apply(lambda x: x[0])
df["group_identifier"] = df.raw_group.apply(lambda x: x[1])

df_grouped = df[["_id",
                 "group",
                 "includes_information",
                 "group_identifier"]].groupby(by=["group",
                                                  "includes_information",
                                                  "group_identifier"]).count()
df_grouped.columns = ['number of records']
df_grouped.to_csv("./data_analysis_output.csv")
